#!/bin/bash

set -e

User="sunwyl"
echo "User :$User"
Password="Sun.qwe123"
docker login --username ${User} --password ${Password}

WORKING_PATH=`pwd`
BUILD_ID="ubuntu-18.04"
version=${BUILD_ID}-$(date "+%Y%m%d")
echo "version :$version"

cd ${WORKING_PATH}

echo "WORKING PATH: ${WORKING_PATH}"

ls ${WORKING_PATH}

UBUNTU_IMAGE="sunwyl/linux_sys:${version}"

echo "ubuntu image: ${UBUNTU_IMAGE}"

docker build . \
-f ./Dockerfile \
-t ${UBUNTU_IMAGE}

docker push ${UBUNTU_IMAGE}